﻿using System;
using System.Collections.Generic;
using static Vehicles.CarBody;
using static Vehicles.SorterVehicleType;

namespace Vehicles {

    class Program {

        static void Main( string[] args ){
            Console.WriteLine("Vehicles");

	    List<Vehicle> vehicles = PrepareCollection();
	    PrintList( vehicles );

	    PrintSortedVehicle( vehicles, MaxSpeed );
	    PrintSortedVehicle( vehicles, AllFields );
        }

	private static List<Vehicle> PrepareCollection(){
		Car c1 = new Car( 2017, 220, new DateTime( 2017, 5, 21 ), Coupe );
		Car c2 = new Car( 2019, 240, new DateTime( 2017, 2, 6 ), Limousine );
		Car c3 = new Car( 2019, 240, new DateTime( 2019, 1, 1 ), Hatchback );

		Motorcycle m1 = new Motorcycle( 2016, 260, new DateTime( 2016, 7, 14 ), false );
		Motorcycle m2 = new Motorcycle( 2019, 330, new DateTime( 2019, 5, 13 ), false );
		Motorcycle m3 = new Motorcycle( 2019, 240, new DateTime( 2018, 9, 1  ), true  );

		List<Vehicle> vehicles = new List<Vehicle>{ c1, c2, c3, m1, m2, m3 };

		return vehicles;
	}

	private static void PrintList<T>( List<T> items ){
		Console.WriteLine("items -> ");
		foreach( T item in items ){
			Console.WriteLine( item );
		}
	}

	private static void PrintSortedVehicle( List<Vehicle> vehicles, SorterVehicleType sorterType ){

		switch( sorterType ){
			case MaxSpeed:
				vehicles.Sort( new VehicleMaxSpeedComparator() );
				break;

			case AllFields:
				vehicles.Sort( new VehicleAllFieldsComparator() );
				break;
			default:
				throw new NotImplementedException( String.Format( "Unknown sorter type: {0}", sorterType ) );				
		}
		PrintList( vehicles );
	}

    }
}
