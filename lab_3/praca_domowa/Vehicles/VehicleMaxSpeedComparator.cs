﻿using System;
using System.Collections.Generic;

namespace Vehicles {

    class VehicleMaxSpeedComparator: IComparer<Vehicle> {

	    public int Compare( Vehicle v1, Vehicle v2 ){
		    if( v1 == null && v2 == null ){
			    return 0;

		    }else if( v1 == null && v2 != null ){
			    return -1;

		    }else if( v1 != null && v2 == null ){
			    return 1;

		    }else{
			    return v1.MaxSpeed.CompareTo( v2.MaxSpeed );
		    }
	    }
    }
}
