﻿namespace Vehicles {
   
    public enum CarBody {
	    Cabriolet, Coupe, Hatchback, Kombi, Limousine
    }
}
