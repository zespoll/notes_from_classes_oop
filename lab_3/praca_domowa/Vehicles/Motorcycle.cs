﻿using System;

namespace Vehicles {

    class Motorcycle: Vehicle {

	    public bool PassengerSeat {get;}

	    public Motorcycle( int yearOfProduction, int maxSpeed, DateTime dateOfFirstRegistration, bool passengerSeat ):
		    base( yearOfProduction, maxSpeed, dateOfFirstRegistration ){
		    this.PassengerSeat = passengerSeat;
	    }

	    public override string ToString(){
		    return String.Format( "Motorcycle [yearOfProduction: {0}; maxSpeed: {1}, dateOfFirstRegistration: {2:d}, passengerSeat: {3}]", 
				    YearOfProduction, MaxSpeed, DateOfFirstRegistration, PassengerSeat );
	    }

    }
}
