﻿using System;
using System.Collections.Generic;

namespace Vehicles {

    class VehicleAllFieldsComparator: IComparer<Vehicle> {

	    public int Compare( Vehicle v1, Vehicle v2 ){
		    int result;
		    if( v1 == null && v2 == null ){
			    result = 0;

		    }else if( v1 == null && v2 != null ){
			    result = -1;

		    }else if(v1 != null && v2 == null ){
			    result = 1;

		    }else if( v1.MaxSpeed.CompareTo( v2.MaxSpeed ) != 0 ){
			    result = v1.MaxSpeed.CompareTo( v2.MaxSpeed );

		    }else if( v1.YearOfProduction.CompareTo( v2.YearOfProduction ) != 0 ){
			    result = v1.YearOfProduction.CompareTo( v2.YearOfProduction );

		    }else{
			    result = v1.DateOfFirstRegistration.CompareTo( v2.DateOfFirstRegistration );
		    }

		    return result;
	    }
    }
}
