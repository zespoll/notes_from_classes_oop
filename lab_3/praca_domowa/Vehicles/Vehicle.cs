﻿using System;

namespace Vehicles {

    abstract class Vehicle {
	    
	    public int YearOfProduction {get;}
	    public int MaxSpeed {get;}
	    public DateTime DateOfFirstRegistration {get;}

	    public Vehicle( int yearOfProduction, int maxSpeed, DateTime dateOfFirstRegistration ){
		    this.YearOfProduction = yearOfProduction;
		    this.MaxSpeed = maxSpeed;
		    this.DateOfFirstRegistration = dateOfFirstRegistration;
	    }
    }
}
