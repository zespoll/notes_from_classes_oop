﻿using System;

namespace Vehicles {

    class Car: Vehicle {

	    public CarBody Body {get;}

	    public Car( int yearOfProduction, int maxSpeed, DateTime dateOfFirstRegistration, CarBody body ):
		    base( yearOfProduction, maxSpeed, dateOfFirstRegistration ){
            	    this.Body = body;
	    }

	    public override string ToString(){
		    return String.Format( "Car [yearOfProduction: {0}, maxSpeed: {1}, dateOfFirstRegistration: {2:d}, body: {3}]",
				    YearOfProduction, MaxSpeed, DateOfFirstRegistration, Body);
	    }
    }
}
