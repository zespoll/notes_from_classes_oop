﻿using System;

namespace ShoppingSundaySpace
{
    class ShoppingInfo
    {
        static void Main(string[] args)
        {
            int nDaysToSunday = CountDaysToNearestShoppingSunday();
            Console.WriteLine($"Start next (Sunday) shopping in {nDaysToSunday} days");
        }

        private static int CountDaysToNearestShoppingSunday()
        {
            DateTime now = DateTime.Now;
            DateTime lastSunday = GetDateOfLastSundayInMonth(now.Month);

            if (lastSunday.Day < now.Day)
            {
                lastSunday = GetDateOfLastSundayInMonth(now.Month + 1);
            }

            int nDaysToSunday = (lastSunday.Date - now.Date).Days;

            return nDaysToSunday;
        }

        private static DateTime GetDateOfLastSundayInMonth(int month)
        {
            DateTime now = DateTime.Now;
            int daysInMonth = DateTime.DaysInMonth(now.Year, month);
            DateTime lastDay = new DateTime(now.Year, month, daysInMonth);
            DateTime lastSunday = lastDay.AddDays(-(int)lastDay.DayOfWeek);

            return lastSunday;
        }
    }
}