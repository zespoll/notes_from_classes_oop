# SOLID
SOLID to akronim, którego autorstwo przypisuje się panu Robertowi C. Martinowi (Uncle Bob). Każda litera oznacza jedną z pięciu dobrych zasad / reguł / praktyk tworzenia oprogramowania.

### Zaczynamy od brzydkiego kodu.
Zadaniem jest stworzeni klasy `Student`, która będzie zawierała kilka pól opisujących studenta oraz metody weryfikujące status ważności rejestracji. Ważność rejestracji jest sprawdzana poprzez porównanie ustawionej daty ważności rejestracji z datą aktualną.

```cs
using System;
using System.Collections.Generic;

namespace Wrong_Single_Responsibility_Principle
{
    class Student
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public DateTime CreationDate { get; }
        public DateTime RegistrationValidityDate { get; set; }
        public List<StudentSubject> Grades { get; }

        public Student(string name, string surname, int age)
        {
            this.Name = name;
            this.Surname = surname;
            this.Age = age;
            this.CreationDate = DateTime.Now;

            InitializeRegistrationForOneYear();
        }

        private void InitializeRegistrationForOneYear()
        {
            this.RegistrationValidityDate = DateTime.Now.AddYears(1);
        }

        public bool CheckValidation()
        {
            return RegistrationValidityDate >= DateTime.Now;
        }
    }

    class StudentSubject
    {
        public int SubjectId { get; }
        public Enum Grade { get; set; }
    }

    enum Grade
    {
        A = 5, B = 4, C = 3, D = 2
    }
}
```

### SRP (Single Responsibility Principle)
**Znaczenie: każdy moduł (klasa) powinien mieć tylko jedną przyczynę zmian. A patrząc bardziej z punktu widzenia projektowania oprogramowania: każdy moduł powinien być zależny tylko od jednego aktora.**

Załóżmy, że Dziekan ds. nauki potrzebuje funkcjonalności (metody) do wyliczenia średniej rankingowej, na podstawie ocen uzyskanych przez studentów (`grades`). Warunek jest jednak taki, że jeśli student nie ma ważnej rejestracji (`CheckValidation()` zwróci `false`), to średnia rankingowa będzie równa 0.

Dodatkowo Pani z Dziekanatu potrzebuje funkcjonalności (metody) do przeprowadzenia rejestracji studentów na kolejny semestr. Ale to może nastąpić tylko wtedy, jeżeli student podpisał aneks do umowy.

W tym momencie pojawiają się dwa zadania.
1. Wyliczenie średniej rankingowej.
2. Zapisanie studenta na kolejny semestr.

Te dwa zadania można rozdzielić pomiędzy dwóch programistów. Pierwszy programista skorzysta z istniejącej metody `CheckValidation`.
```cs
public double CountRanking()
{
    double result = 0;
    if (CheckValidation())
    {
        result = CountMidGrade();
    }

    return result;
}

private double CountMidGrade()
{
    double midGrade = 0;
    foreach(StudentSubject subject in Grades){
        midGrade += (int)subject.Grade;
    }
    midGrade /= Grades.Count;

    return midGrade;
}
```

Natomiast drugi programista może postanowić dodać nowe pola (`CurrentSemester`, `AnnexSigned`) oraz zmodyfikować metodę `CheckValidation` w taki sposób, aby oprócz daty ważności rejestracji, sprawdzała jeszcze status podpisania aneksu do umowy.
```cs
public bool CheckValidation()
{
    return AnnexSigned && RegistrationValidityDate >= DateTime.Now;
}

public void EnrollForNextSemesterIfAllIsCorrect(int currentSemester)
{
    if (CheckValidation() && CurrentSemester == currentSemester)
    {
        this.CurrentSemester++;
    }
    else
    {
        throw new EnrollingForNextSemesterException(String.Format("Cannot enroll student [studentId: {0}, currentSemester: {1}]", Id, currentSemester));
    }
}
```
Od tego momentu kod będzie działał zgodnie z oczekiwaniami Pani z Dziekanatu, ale Dziekan ds. nauki będzie miał błędnie działającą obsługę liczenia średniej rankingowej.

W tym momencie od razu widać, że zasada pojedynczej odpowiedzialności zostanie naruszona. Dzieje się tak, ponieważ odpowiedzialność klasy `Student` jest zbyt duża. Ta klasa odpowiada przed kilkoma aktorami (przed Panią z Dziekanatu i przed Dziekanem ds. nauki). Z tego powodu wszystkie metody powinny zostać wyniesione do osobnych serwisów funkcjonalnych (odpowiadających za określony zakres).

W efekcie końcowym kod można podzielić na wiele osobnych składowych.
##### Student.cs
```cs
using System;
using System.Collections.Generic;

namespace Single_Responsibility_Principle
{
    class Student
    {
        public long Id { get; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }
        public DateTime CreationDate { get; }
        public DateTime RegistrationValidityDate { get; set; }
        public List<StudentSubject> Grades { get; }
        public int CurrentSemester { get; set; }
        public bool AnnexSigned { get; set; }

        public Student(long id, string name, string surname, int age)
        {
            this.Id = id;
            this.Name = name;
            this.Surname = surname;
            this.Age = age;
            this.CreationDate = DateTime.Now;

            InitializeRegistrationForOneYear();
        }

        private void InitializeRegistrationForOneYear()
        {
            this.RegistrationValidityDate = DateTime.Now.AddYears(1);
        }
    }
}
```

##### StudentRankgingService.cs

```cs
namespace Single_Responsibility_Principle
{
    interface StudentRankingService
    {
        double CountRanking(Student student);
    }
}
```

##### StudentRankging.cs

```cs
using System;

namespace Single_Responsibility_Principle
{
    class StudentRanking : StudentRankingService
    {
        public double CountRanking(Student student)
        {
            double result = 0;
            if (CheckValidation(student))
            {
                result = CountMidGrade(student);
            }

            return result;
        }

        private double CountMidGrade(Student student)
        {
            double midGrade = 0;
            foreach (StudentSubject subject in student.Grades)
            {
                midGrade += (int)subject.Grade;
            }
            midGrade /= student.Grades.Count;

            return midGrade;
        }

        public bool CheckValidation(Student student)
        {
            return student.RegistrationValidityDate >= DateTime.Now;
        }
    }
}
```

##### StudentRegistrationService.cs

```cs
namespace Single_Responsibility_Principle
{
    interface StudentRegistrationService
    {
        void EnrollForNextSemesterIfAllIsCorrect(int currentSemester, Student student);
    }
}
```

##### StudentRegistration.cs

```cs
using System;

namespace Single_Responsibility_Principle
{

    class StudentRegistration : StudentRegistrationService
    {
        public void EnrollForNextSemesterIfAllIsCorrect(int currentSemester, Student student)
        {
            if (CheckValidation(student) && student.CurrentSemester == currentSemester)
            {
                student.CurrentSemester++;
            }
            else
            {
                throw new EnrollingForNextSemesterException(
                    String.Format("Cannot enroll student [studentId: {0}, currentSemester: {1}]",
                    student.Id, currentSemester));
            }
        }

        public bool CheckValidation(Student student)
        {
            return student.AnnexSigned && student.RegistrationValidityDate >= DateTime.Now;
        }
    }
}
```

##### Grade.cs

```cs
namespace Single_Responsibility_Principle
{
    enum Grade
    {
        A = 5, B = 4, C = 3, D = 2
    }
}
```

##### EnrollingForNextSemesterException.cs

```cs
using System;

namespace Single_Responsibility_Principle
{

    class EnrollingForNextSemesterException : Exception
    {
        public EnrollingForNextSemesterException(string message)
        : base(message) { }
    }
}
```

### OCP (Open Closed Principle)
**Znaczenie: element oprogramowania powinien być otwarty na rozbudowę, ale zamknięty na modyfikację.**

Działanie tej reguły powinno być jasne. Możemy dodawać nowe funkcjonalności do naszego oprogramowania, ale nie powinniśmy modyfikować tego, co już jest. Tzn. jeśli w tym, co "już" mamy, jest błąd, to oczywiście trzeba to naprawić (czyli modyfikować). Ale jeśli to, co "już" mamy, działa poprawnie, a pojawia się potrzeba rozwoju naszego systemu (wprowadzenia dodatkowych (nowych) funkcjonalności), to nie wolno nam modyfikować starych funkcjonalności. Jeśli kod jest dobrze zaprojektowany / zaimplementowany, to nie powinna pojawić się potrzeba modyfikacji.

Założyliśmy, że chcemy mieć grupę studentów trzeciego roku, dla których sprawdzanie ważności rejestracji powinno dodatkowo (oprócz tego, co już robi) weryfikować ważność badania lekarskiego.
Dlatego zapadła decyzja o rozszerzeniu klasy `Student` o dodatkowe pole.

##### ThirdAgeStudent.cs

```cs
using System;
using Single_Responsibility_Principle;

namespace Open_Closed_Principle
{
    class ThirdAgeStudent : Student
    {
        public DateTime MedicalAuthorizationDate { get; set; }
        public ThirdAgeStudent(long id, string name, string surname, int age)
        : base(id, name, surname, age) { }
    }
}
```

##### ThirdAgeStudentRegistration.cs

```cs
using System;
using Single_Responsibility_Principle;

namespace Open_Closed_Principle
{
    class ThirdAgeStudentRegistration : StudentRegistration
    {
        public bool CheckValidation(ThirdAgeStudent student)
        {
            bool medicalAuthorizationIsValid = student.MedicalAuthorizationDate >= DateTime.Now;
            
            return base.CheckValidation(student) && medicalAuthorizationIsValid;
        }
    }
}
```

### LSP (Liskov Substitution Principle)
**Znaczenie: interpretacja pierwotnej myśli Barbary Liskov ewoluowała przez lata. Dzisiaj można to uprościć do stwierdzenia: jeżeli klasa bazowa (lub interfejs) ma X metod, to klasa pochodna też musi implementować te same metody w taki sposób, aby pod klasę bazową można było podstawić klasy pochodne.**

Załóżmy, że zostanie zdefiniowany interfejs z deklaracjami dwóch metod. Jeżeli jedna klasa zaimplementuje obydwie metody, to kolejne klasy również powinny poprawnie implementować obydwie metody. Jeżeli ten warunek jest spełniony, to powinniśmy móc zamienić jedną implementację z drugą, a program dalej powinien działać.
Poniżej został pokazany przykład niespełniający tej zasady (LSP).
Jest interfejs z dwoma metodami. Ale jedna z klas implementuje poprawnie tylko jedną metodę. Przy próbie wywołania drugiej metody, zostanie rzucony wyjątek. Czyli program różnie się zachowa w zależności od podstawienia (użycia) konkretnej implementacji. Klasa `StudentViewShortInfo`, choć (teoretycznie) implementuje interfejs `StudentView`, nie może być podstawiona w miejsce `StudentViewWithSpace`, ponieważ program może zachować się inaczej przy wywołaniu metody `PrintOnlyNames`.

##### StudentView.cs

```cs
using System.Collections.Generic;
using Single_Responsibility_Principle;

namespace Liskov_Substitution_Princile
{
    interface StudentView
    {
        void PrintAllInfo(List<Student> students);
        void PrintOnlyNames(List<Student> students);
    }
}
```

##### StudentViewWithSpace.cs

```cs
using System;
using System.Collections.Generic;
using Single_Responsibility_Principle;

namespace Liskov_Substitution_Princile
{
    class StudentViewWithSpace : StudentView
    {
        public void PrintAllInfo(List<Student> students)
        {
            Console.WriteLine($"Name \t\t Surname \t\t Age \t\t CreationDate");
            foreach (Student student in students)
            {
                Console.WriteLine($"{student.Name} \t\t {student.Surname} \t\t {student.Age} \t\t {student.CreationDate}");
            }
        }

        public void PrintOnlyNames(List<Student> students)
        {
            Console.WriteLine($"Student name");
            foreach (Student student in students)
            {
                Console.WriteLine($"{student.Name}");
            }
        }
    }
}
```

##### StudentViewShortInfo.cs

```cs
using System;
using System.Collections.Generic;
using Single_Responsibility_Principle;

namespace Liskov_Substitution_Princile
{
    class StudentViewShortInfo : StudentView
    {
        public void PrintAllInfo(List<Student> students)
        {
            foreach (Student student in students)
            {
                Console.WriteLine($"Student[{student.Name} {student.Surname} {student.Age} {student.CreationDate}]");
            }
        }

        public void PrintOnlyNames(List<Student> students)
        {
            throw new System.NotImplementedException();
        }
    }
}
```


### ISP (Interface Segregation Principle)
**Znaczenie: interfejsy powinny zawierać tylko takie deklaracje, które są ze sobą ściśle powiązane i ich umiejscowienie w jednym miejscu (jednym pliku) ma sens.**

Na przykładzie poprzedniego punktu widzimy, że interfejs nie spełniał tej zasady, ponieważ klasa implementująca nie potrzebowała wszystkich metod. Aby naprawić ten błąd (choć niekoniecznie naprawimy błąd LSP), wystarczy rozbić interfejs na dwa składowe interfejsy (po jednej metodzie w każdym interfejsie).

Choć ten przykład jest dość sztuczny. To jednak należy zapamiętać, że jeżeli klasa implementująca interfejs nie musi / nie będzie wykorzystywać wszystkich implementowanych metod, to oznacza, że interfejs jest zbyt duży (za mało specjalizowany). Innymi słowy: lepiej mieć kilka (uporządkowanych) interfejsów, zamiast jednego dużego interfejsu, który zawiera w sobie zbyt dużo deklaracji.


### DIP (Dependency Inversion Principle)
**Znaczenie: powinniśmy unikać zależności od konkretnych implementacji. Kod powinien być projektowany w taki sposób, aby w jak największym stopniu zależał od abstrakcji a nie od konkretnych elementów.**

Kto był obecny i słuchał na zajęciach, powinien wiedzieć, jak należy interpretować zasadę odwrócenia zależności.
