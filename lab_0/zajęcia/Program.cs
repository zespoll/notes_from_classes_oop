﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");

            string name = "Pawel";
            Console.WriteLine("Cześć, " + name + "!");
            Console.WriteLine( $"Cześć, {name}!" );

            string lyrics = "   To jest tekst fajnej piosenki do śpiewania.   ";
            Console.WriteLine( $"[{lyrics}]" );
            Console.WriteLine($"[{lyrics.TrimEnd()}]");
            Console.WriteLine($"[{lyrics.TrimStart()}]");
            Console.WriteLine($"[{lyrics.Trim()}]");

            string trimLyrics = lyrics.Trim();
            string newLyrics = trimLyrics.Replace("fajnej", "niefajnej");
            Console.WriteLine(newLyrics);

            string surname = "Zawadzki";
            Console.WriteLine($"ToUpper: {surname.ToUpper()}");
            Console.WriteLine($"ToLower: {surname.ToLower()}");

            Console.WriteLine($"Czy zawiera 'tekst'? -> {trimLyrics.Contains("tekst")}");
            Console.WriteLine($"Czy zaczyna od 'To'? -> {trimLyrics.StartsWith("To")}");

            int a = 29;
            int b = 7;
            int c = 29 / b;
            Console.WriteLine($"{a}/{b}={c}");
            Console.WriteLine($"9 mod 5 = {9 % 5}");

            Console.WriteLine($"int max = {int.MaxValue}");

            int d = 65536;
            Console.WriteLine($"d*d / 2 - 1 = {d*d / 2 - 1}");
            Console.WriteLine($"d / 2 * d - 1 = {d / 2 * d - 1}");
            Console.WriteLine($"d*d = {d * d}");
            Console.WriteLine($" d / 2 * d = {d / 2 * d}");
            Console.WriteLine($"int min = {int.MinValue}");

            double e = 40.0;
            double f = 15.0;
            double g = e / f;
            Console.WriteLine($"{e}/{f}={g}");

            decimal h = decimal.MaxValue;
            Console.WriteLine($"decimal max = {h}");

            decimal o = 1.0M;
            Console.WriteLine($"o = {o}");

            string[] names = { "Paweł", "Bartek", "Jacek", "Robert", "Wiktor" };
            for (int i = 0; i < names.Count(); i++)
            {
                Console.WriteLine($"name[{i+1}] = {names[i]}");
            }
                        
            foreach (string namee in names)
            {
                Console.WriteLine($"name -> {namee}]");
            }

            var nums = new List<int> { 4, 2, 1, 9 };
            nums.Add(12);
            nums.Add(0);

            foreach(int num in nums)
            {
                Console.WriteLine(num);
            }
            nums.Sort();
            foreach (int num in nums)
            {
                Console.WriteLine(num);
            }
       }
    }
}
