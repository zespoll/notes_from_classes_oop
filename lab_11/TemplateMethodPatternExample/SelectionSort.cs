class SelectionSort : Sorter
{
    protected override void SortData(double[] nums)
    {
        int n = nums.Length;
        int maxValueIndex;

        for (int i = n - 1; i >= 0; i--)
        {
            maxValueIndex = i;
            for (int j = 0; j < i; j++)
            {
                if (nums[j] > nums[maxValueIndex])
                {
                    maxValueIndex = j;
                }
            }
            swap(nums, i, maxValueIndex);
        }
    }

    private void swap(double[] nums, int id1, int id2)
    {
        if (id1 == id2)
        {
            return;
        }
        double tmpValue = nums[id1];
        nums[id1] = nums[id2];
        nums[id2] = tmpValue;
    }
}