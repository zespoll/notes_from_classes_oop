using System;

abstract class Sorter
{
    private int paddingValue = 9;

    public void ComplexSort(double[] nums)
    {
        if (!ValidateData(nums))
        {
            return;
        };
        SortData(nums);
        DisplayData(nums);
    }

    protected bool ValidateData(double[] nums)
    {
        if (nums == null)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    protected abstract void SortData(double[] nums);

    protected void DisplayData(double[] nums)
    {
        int step = 10;
        int n = nums.Length;
        int line = 0;
        int nextLineBreak;

        for (int i = 0; i < n; i++)
        {
            nextLineBreak = (line + 1) * step - 1;
            if (i % step == 0)
            {
                Console.Write($"[{i}-{i + step - 1}]".PadRight(paddingValue) + " =>");
            }
            if (i == nextLineBreak)
            {
                line++;
                Console.Write("\n");
                continue;
            }
            Console.Write($" {nums[i]} |");
        }
        Console.Write("\n");
    }
}
