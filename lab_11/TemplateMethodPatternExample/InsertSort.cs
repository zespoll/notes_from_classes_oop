class InsertSort : Sorter
{
    protected override void SortData(double[] nums)
    {
        int n = nums.Length;
        double valueToSet;
        int j;

        for (int i = 1; i < n; i++)
        {
            j = i;
            valueToSet = nums[i];
            for (; j - 1 >= 0 && nums[j - 1] > valueToSet; j--)
            {
                nums[j] = nums[j - 1];
            }
            if (j != i)
            {
                nums[j] = valueToSet;
            }
        }
    }
}