﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleApp2;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            PointCords p1 = new PointCords(1, 2);

            Console.WriteLine( $"PointCords x={p1.x}, y={p1.y}");
            Console.WriteLine("PointCords y={1}, x={0}", p1.x, p1.y);

            PointCordsPriv p2 = new PointCordsPriv(3, 4);
            Console.WriteLine($"PointCordsPriv x={p2.X}, y={p2.Y}");

            B b = new B();
            b.printMe();

            Class1.HelloWorld();
            ConsoleApp2.PointCords p3 = new ConsoleApp2.PointCords(1,2);

            Student stud1 = new Student("Pawel", "Z", 19);
            Student stud2 = new Student("Jacek", "S", 24);
            Student stud3 = new Student("Bartek", "C", 22);
            Student stud4 = new Student("Zuza", "K", 21);

            List<Student> students = new List<Student> { stud1, stud2, stud3, stud4 };

            students.Sort();
            foreach(Student student in students)
            {
                Console.WriteLine(student);
            }

            students.Sort(delegate(Student s1, Student s2)
            {
                if (s1 == null && s2 == null)
                {
                    return 0;
                }
                else if(s1.Surname == null)
                {
                    return -1;
                }else if(s2.Surname == null)
                {
                    return 1;
                }
                else
                {
                    return s1.Surname.CompareTo(s2.Surname);
                }                
            });

            Console.WriteLine("-----------");
            foreach(Student student in students)
            {
                Console.WriteLine(student);
            }

        }
    }

    class PointCords
    {
        public int x;
        public int y;

        public PointCords( int x, int y)
        {
            this.x = x;
            this.y = y;
        }
    }

    class PointCordsPriv
    {
        public int X { get; }
        public int Y { get; }

        public PointCordsPriv( int X, int Y)
        {
            this.X = X;
            this.Y = Y;
        }

        public override string ToString()
        {
            return string.Format("x={0}, y={1}", X, Y);
        }
    }

    class A
    {
        protected int x = 666;
    }

    class B : A
    {
        public void printMe()
        {
            Console.WriteLine("A.x = {0}", x);
        }
    }

    class Student : IComparable
    {
        public string Name { get; set; }
        public string Surname { get; set; }
        public int Age { get; set; }

        public Student( string Name, string Surname, int Age )
        {
            this.Name = Name;
            this.Surname = Surname;
            this.Age = Age;
        }



        public override string ToString()
        {
            return string.Format("Name: {0}, Surname: {1}, Age: {2}", Name, Surname, Age);
        }

        public int CompareTo(object o)
        {
            if( o == null)
            {
                return 1;
            }
            Student studentObj = o as Student;
            if(studentObj != null)
            {
                return this.Name.CompareTo(studentObj.Name);
            }
            else
            {
                throw new ArgumentException("Object is not Student");
            }
            
        }
    }
}
