﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Class1
    {
        public static void HelloWorld()
        {
            Console.WriteLine("Hello, World!");
        }
    }

    class PointCords
    {
        public int x;
        public int y;

        public PointCords( int x, int y )
        {
            this.x = x;
            this.y = y;
        }

    }
}
