﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    class Article : BibItem {

	    public Article( string title, string author, int yearOfPublication )
	    : base( title, author, yearOfPublication ){
	    }

	    public override string ToString(){
		    return String.Format( "Article: [title: {0}, author: {1}, yearOfPublication: {2}]", Title, Author, YearOfPublication );
	    }

    }

}
