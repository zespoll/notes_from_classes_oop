﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    class Book : BibItem {

	    public int EditionId {get;}
	    public string[] Coauthors {get; set;}

	    public Book( string title, string author, int yearOfPublication, int editionId )
	    : base( title, author, yearOfPublication ){
		    this.EditionId = editionId;
	    }

	    public Book( string title, string author, int yearOfPublication, int editionId, string[] coauthors )
	    : base( title, author, yearOfPublication ){
		    this.EditionId = editionId;
		    this.Coauthors = coauthors;
	    }

	    public override string ToString(){
		    string authors = 
			    Coauthors == null
			    ? ""
			    : string.Join( "", Coauthors );

		    return String.Format( "Book: [title: {0}, author: {1}, yearOfPublication: {2}, editionId: {3}, Coauthors: {4}]", Title, Author, YearOfPublication, EditionId, authors );
	    }

    }
}
