﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    abstract class PaperMagazine : BibItem {

	    public int No {get;}
	    public int Pages {get;}
	    
	    public PaperMagazine( string title, string author, int yearOfPublication, int no, int pages )
	    : base( title, author, yearOfPublication ){
		    this.No = no;
		    this.Pages = pages;
	    }

    }
}
