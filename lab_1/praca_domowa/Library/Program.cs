﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    class Program {

        static void Main( string[] args ){
		Library lib = new Library();
		PrepareBooks( lib );
		PrepareJournals( lib );
		PrepareNewsPapers( lib );
		PrepareArticles( lib );

		lib.PrintBooks();
		lib.PrintJournals();
		lib.PrintNewsPapers();
		lib.PrintArticles();

		lib.PrintAll();
        }

	static void PrepareBooks( Library lib ){
		string[] book5Coauthors = {"Kolega Autora", "Drugi Kolega"};

		Book book1 = new Book( "Zły", "Leopold Tyrmand", 1955, 1 );
		Book book2 = new Book( "Wilk stepowy", "Herman Hesse", 1927, 1 );
		Book book3 = new Book( "Trucicielka i inne historie o namiętnościach", "Eric-Emmanuel Schmitt", 2010, 1 );
		Book book4 = new Book( "Bóg nie jest automatem do kawy.", "Marketa Zahradnikova", 2018, 1 );
		Book book5 = new Book( "Bez tytułu.", "Znany Autor", 2018, 1, book5Coauthors );

		lib.Add( book1 );
		lib.Add( book2 );
		lib.Add( book3 );
		lib.Add( book4 );
		lib.Add( book5 );
	}

	static void PrepareJournals( Library lib ){
		Journal journal1 = new Journal( "Wnętrza Inspiracje", "Czasopisma krajowe", 2019, 3, 40, "Kuchnia w salonie" );
		Journal journal2 = new Journal( "M jak Mieszkanie", "Czasopisma krajowe", 2019, 11, 49, "Urządzamy ze smakiem" );
		Journal journal3 = new Journal( "Mój ogród", "Czasopisma krajowe", 2019, 10, 39, "Gołębie w ogrodzie" );

		lib.Add( journal1 );
		lib.Add( journal2 );
		lib.Add( journal3 );
	}

	static void PrepareNewsPapers( Library lib ){
		NewsPaper newsPaper1 = new NewsPaper( "Gazeta Wyborcza", "Agora", 2019, 41, 28, "Dokąd idziemy?" );
		NewsPaper newsPaper2 = new NewsPaper( "Do Rzeczy", "Orle Pióro Sp. z o.o.", 2019, 41, 45, "Twierdza senat" );

		lib.Add( newsPaper1 );
		lib.Add( newsPaper2 );
	}

	static void PrepareArticles( Library lib ){
		Article article1 = new Article( "Einstein vs Weinstein", "Znany Profesor", 2019 );
		Article article2 = new Article( "Percepcja światów możliwych w jednokomórkowcach -- ruchu dziewicze: analiza obserwacji", "Nienznay doktor", 2018 );
		Article article3 = new Article( "Regeneracja DNA u zwierząt", "Początkujący Naukowiec", 2018 );

		lib.Add( article1 );
		lib.Add( article2 );
		lib.Add( article3 );

	}

    }
}
