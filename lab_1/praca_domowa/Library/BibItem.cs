﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    abstract class BibItem {

	    public string Title {get;}
	    public string Author {get;}
	    public int YearOfPublication {get;}

	    public BibItem( string title, string author, int yearOfPublication ){
		    this.Title = title;
		    this.Author = author;
		    this.YearOfPublication = yearOfPublication;
	    }

    }
}
