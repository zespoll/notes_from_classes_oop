﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    class NewsPaper : PaperMagazine {

	    public string MainTopic {get;}

	    public NewsPaper( string title, string author, int yearOfPublication, int no, int pages, string mainTopic )
	    : base( title, author, yearOfPublication, no, pages ){
		    this.MainTopic = mainTopic;
	    }

	    public override string ToString(){
		    return String.Format( "NewsPaper: [title: {0}, author: {1}, yearOfPublication: {2}, no: {3}, pages: {4}, mainTopic: {5}]", Title, Author, YearOfPublication, No, Pages, MainTopic );
	    }
	    
    }
}
