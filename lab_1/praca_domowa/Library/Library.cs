﻿using System;
using System.Collections.Generic;

namespace LibrarySpace {

    class Library {

	    private List<Book> books = new List<Book>();
	    private List<Journal> journals = new List<Journal>();
	    private List<NewsPaper> newsPapers = new List<NewsPaper>();
	    private List<Article> articles = new List<Article>();

	    public void Add( BibItem bibItem ){
		    if( bibItem == null ){
			    throw new System.ArgumentException( "BibItem cannot be null." );
		    }

		    if( bibItem.GetType() == typeof( Book ) ){
			    books.Add( (Book) bibItem );

		    }else if( bibItem.GetType() == typeof( Journal ) ){
			    journals.Add( (Journal) bibItem );

		    }else if( bibItem.GetType() == typeof( NewsPaper ) ){
			    newsPapers.Add( (NewsPaper) bibItem );
		    

		    }else if( bibItem.GetType() == typeof( Article ) ){
			    articles.Add( (Article) bibItem );

		    }else{
			    throw new System.ArgumentException( "Unknown type of bib item." );
		    }

	    }

	    public void PrintBooks(){
		    books.Sort( (book1, book2) => book1.Title.CompareTo( book2.Title ) );
		    PrintItems<Book>( books );
	    }

	    public void PrintJournals(){
		    journals.Sort( (journal1, journal2) => journal1.No.CompareTo( journal2.No ) );
		    PrintItems<Journal>( journals );
	    }

	    public void PrintNewsPapers(){
		    newsPapers.Sort( (news1, news2) => news1.MainTopic.CompareTo( news2.MainTopic ) );
		    PrintItems<NewsPaper>( newsPapers );
	    }

	    public void PrintArticles(){
		    articles.Sort( (art1, art2) => art1.Author.CompareTo( art2.Author ) );
		    PrintItems<Article>( articles );
	    }

	    public void PrintAll(){
		    List<BibItem> bibItems = new List<BibItem>();
		    bibItems.AddRange( books );
		    bibItems.AddRange( journals );
		    bibItems.AddRange( newsPapers );
		    bibItems.AddRange( articles );

		    bibItems.Sort( (b1, b2) => b1.Title.CompareTo( b2.Title ) );

		    PrintItems<BibItem>( bibItems );
	    }

	    private void PrintItems<T>( List<T> list ){
		    Console.WriteLine("\nAll {0}s:", typeof(T).Name );
		    foreach( T bibItem in list ){
			    Console.WriteLine( bibItem );
		    }
	    }

    }
}
