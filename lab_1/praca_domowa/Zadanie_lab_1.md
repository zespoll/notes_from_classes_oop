### Zadanie domowe: biblioteka
1. Proszę stworzyć bibliotekę (z książkami).
2. Biblioteka przechowuje byty biblioteczne (książki, czasopisma, artykuły (?), nagrania (?)).
3. Istnieje możliwość dodawania nowych bytów do biblioteki.
4. Powinniśmy mieć możliwość sortować wszystkie byty po jakimś wspólnym polu (np. dacie albo tytule).
5. Powinna być możliwość sortowania samych książek , czasopism i pozostałych bytów.
6. Powinna być możliwość wylistowania (wypisania) danej kategorii (np. samych książek) jak i wszystkich bytów.

