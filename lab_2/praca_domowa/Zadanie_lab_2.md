### Zadanie 1
Proszę stworzyć metodę, która zwróci informację o liczbie dni pozostałych do "najbliższej" (nie działa wstecz) niedzieli handlowej.

### Zadanie 2
Proszę stworzyć hierachię klas dla Pojazdów.

* `Pojazd` -> `Samochód`,
* `Pojazd` -> `Motocykl`.

Proszę stworzyć kolekcję Pojazdów (różne samochody (x3), różne motocykle (x3)).

Proszę stworzyć metodę, która w zależności od przyjętego parametru będzie sortować kolekcję (listę pojazdów) "po założonym polu" (czyli w sposób określony przez komparator).
Metody sortujące powinny wykorzystywać własne (zaimplementowane przez Państwa) klasy z interfejsem `IComparer<>`.
Pierwszy komparator powinien wykorzytywać wszystkie (trzy) pola z klasy bazowej.
Drugi komparator powinien korzystać tylko z jednego pola klasy bazowej.
