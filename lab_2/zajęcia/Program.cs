﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Student stud1 = new Student("Pawel", "Z", 18);
            Student stud2 = new Student("Jacek", "S", 25);
            Student stud3 = new Student("Ela", "G", 27);

            List<Student> students = new List<Student>{stud1, stud2, stud3};
            PrintList(students);
            StudentNameComparator studNameComparator = new StudentNameComparator();
            students.Sort(studNameComparator);
            PrintList(students);

            Console.WriteLine( DateTime.Now.ToString("dd/MM/yyyy") );
            int year = DateTime.Now.Year;
            int month = DateTime.Now.Month;

            Console.WriteLine(year);
            Console.WriteLine(month);
            Console.WriteLine(DateTime.DaysInMonth(year, month));

            Console.WriteLine( DateTime.Now.ToString("dddd, dd/MM/yyyy HH:mm:ss") );

        }

        static void PrintList<T>( List<T> items)
        {
            foreach( T item in items)
            {
                Console.WriteLine(item);
            }
            Console.WriteLine();
        }

    }

    class Student
    {
        public string Name { get; }
        public string Surname { get; }
        public int Age { get; }

        public Student( string name, string surname, int age )
        {
            this.Name = name;
            this.Surname = surname;
            this.Age = age;
        }

        public override string ToString()
        {
            return String.Format("Student: [name: {0}, surname: {1}, age: {2}]",
                Name, Surname, Age);
        }

    }

    class StudentNameComparator : IComparer<Student>
    {
        public int Compare(Student s1, Student s2)
        {
            if (s1 == null && s2 == null)
            {
                return 0;

            }else if(s1 == null && s2 != null)
            {
                return -1;

            }else if(s1 != null && s2 == null)
            {
                return 1;
            }
            else
            {
                return s1.Name.CompareTo(s2.Name);
            }
        }
    }

}
